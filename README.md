# SSH远程运行插件


![](./python/_images/featured.png)

---------------------------------------------------------

## Table of Contents

* [URL](#url)
* [Summary](#summary)
* [Blocks](#blocks)
* [License](#license)
* [Supported targets](#Supportedtargets)

## URL
* Project URL : ```https://gitee.com/liliang9693/ext-sshtool```

* Tutorial URL : ```https://mindplus.dfrobot.com.cn/extensions-user```

    




## Summary
使用Mind+V1.7.1及以上版本用户库加载此扩展，可实现将当前图形化代码通过ssh方式传输至远程计算机（例如树莓派）运行。

## Blocks

![](./arduinoC/_images/blocks.png)



## Examples

![](./arduinoC/_images/example.png)

## License

MIT

## Supported targets

MCU                | JavaScript    | Arduino   | MicroPython    | Python 
------------------ | :----------: | :----------: | :---------: | -----
arduino        |             |              |             | 
micro:bit        |             |              |             | 
esp32        |             |              |             | 

## Release Logs

* V0.0.1  基础功能完成

