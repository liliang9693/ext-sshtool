#  -*- coding: UTF-8 -*-

str1="import filetocode2"  #起始标记
str2=").run()"  #结束标记
data="" #转换后的代码

print("++++++++++++++++++")
print("file read")

with open(".cache-file.py", "r") as f:  # 打开图形化生成的代码
    data = f.read()  # 读取文件

#print("*****")
#print(data)
#print("-----")
print("coding")

mk1=data.find(str1) #获取起始位置
mk2=data.find(str2)+len(str2) #获取结束位置

data=data[:mk1]+data[mk2:] #去除传输至pi的代码


#将处理后的代码写到另外一个文件，方便传输到树莓派
with open("code.py","w") as f: 
    f.write(data)  # 写文件

print("file write")


