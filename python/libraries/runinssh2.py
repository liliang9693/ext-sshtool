#  -*- coding: UTF-8 -*-

import paramiko
import time
import sys


class RUNSSH2:
    __IP='raspberrypi.local'
    __PORT=22
    __USERNAME='pi'
    __PASSWORD='raspberry'
    __FILEDIR='/home/pi/Desktop/'

    def __init__(self,ip,port,username,password,filedir):
        self.IP=ip
        self.PORT=port
        self.USERNAME=username
        self.PASSWORD=password
        self.FILEDIR=filedir

    def debugprint(self,varl):
        print(varl)

    def filecopy(self):
        
        self.debugprint("ssh:"+self.IP+":"+str(self.PORT))
        
        transport = paramiko.Transport(self.IP, self.PORT)
        transport.connect(username=self.USERNAME, password=self.PASSWORD)
        
        sftp = paramiko.SFTPClient.from_transport(transport)#如果连接需要密钥，则要加上一个参数，hostkey="密钥"
        self.debugprint("copy file...")
        sftp.put('code.py', self.FILEDIR+'mindpluscode.py')#将本地的Windows.txt文件上传至服务器
        transport.close()#关闭连接
        self.debugprint("copy done")

    def filerun(self):
        self.debugprint("ssh:"+self.IP)
        # 实例化一个ssh客户端对象
        ssh = paramiko.SSHClient()
        # 自动在know_host中添加自己要连的机器，忽略这个验证，注意传入的参数paramiko.AutoAddPolicy()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        # 连接信息
        ssh.connect(hostname=self.IP, port=self.PORT, username=self.USERNAME, password=self.PASSWORD)
        self.debugprint("kill python")
        # 传输命令，这里有三个返回值，按顺序第一个是标准输入，即你输入的命令，第二个标准输出，即命令返回值
        # 第三个标准错误，报错的时候会返回给这个值，同时返回值不是固定而是变化的时候也会返回给这个值一个错误
        #stdin, stdout, stderr = ssh.exec_command('ps -ef |grep python3|cut -c 9-15|xargs kill -9') #停止正在运行的python程序
        #stdin, stdout, stderr = ssh.exec_command('pkill -9 python3') #停止正在运行的python程序
        stdin, stdout, stderr = ssh.exec_command("ps -ef | grep mindpluscode.py | awk '{print $2}' | xargs kill -9") #停止正在运行的mindpluscode程序
        time.sleep(2)
        self.debugprint("run python")
        stdin, stdout, stderr = ssh.exec_command('cd '+self.FILEDIR+'; python3 mindpluscode.py > ~/Desktop/log.txt') 
        
        #stdin, stdout, stderr = ssh.exec_command('cd '+self.FILEDIR+'; python3 mindpluscode.py &') 
        '''
        self.debugprint("#root@RVBoards received: ")
        ##返回信息
        out,err = stdout.read().decode('utf-8'),stderr.read().decode('utf-8')
        if err:
            print(err)
        else:
            print(out)
        
        self.debugprint("----------")
        '''
        # 关闭客户端
        ssh.close()
        self.debugprint("run done")

    def run(self):
        self.debugprint(">>>>>>>>>>>>>>>>>>")
        self.filecopy() #拷贝文件到远程主机
        self.filerun()  #运行程序
        self.debugprint("==================")
        sys.exit(0) #退出程序，不让继续运行后续图形化生成的代码