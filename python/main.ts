
//% color="#cf1256" iconWidth=50 iconHeight=40
namespace sshtools{

    
    //% block="ssh begin [IP] [PORT] [USERNAME] [PASSWORD][FILEDIR]" blockType="command"
    //% IP.shadow="string" IP.defl="raspberrypi.local"
    //% PORT.shadow="string" PORT.defl="22"
    //% USERNAME.shadow="string" USERNAME.defl="pi"
    //% PASSWORD.shadow="string" PASSWORD.defl="raspberry"
    //% FILEDIR.shadow="string" FILEDIR.defl="/home/pi/Desktop/""

    export function SshInit_rp(parameter: any, block: any) {
        let ip=parameter.IP.code;
        let port=parameter.PORT.code;
        let username=parameter.USERNAME.code;
        let password=parameter.PASSWORD.code;
        let filedir=parameter.FILEDIR.code;
        
        Generator.addImport(`\nimport filetocode2 \nfrom runinssh2 import RUNSSH2 \nRUNSSH2(${ip},${replaceQuotationMarks(port)},${username},${password},${filedir}).run()\n`)

        
        
    }

  
    //% block="ssh begin [IP] [PORT] [USERNAME] [PASSWORD][FILEDIR]" blockType="command"
    //% IP.shadow="string" IP.defl="rvboards"
    //% PORT.shadow="string" PORT.defl="22"
    //% USERNAME.shadow="string" USERNAME.defl="root"
    //% PASSWORD.shadow="string" PASSWORD.defl="rvboards"
    //% FILEDIR.shadow="string" FILEDIR.defl="/root/Desktop/"

    export function SshInit_rvboard(parameter: any, block: any) {
        let ip=parameter.IP.code;
        let port=parameter.PORT.code;
        let username=parameter.USERNAME.code;
        let password=parameter.PASSWORD.code;
        let filedir=parameter.FILEDIR.code;
        
        Generator.addImport(`\nimport filetocode2 \nfrom runinssh2 import RUNSSH2 \nRUNSSH2(${ip},${replaceQuotationMarks(port)},${username},${password},${filedir}).run()\n`)

        
        
    }
    
    function replaceQuotationMarks(str:string){
            str=str.replace(/"/g, ""); //去除所有引号
            return str
    }


    
}
